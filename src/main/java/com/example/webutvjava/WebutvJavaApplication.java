package com.example.webutvjava;

import com.example.webutvjava.domain.entity.Beverage;
import com.example.webutvjava.domain.entity.Company;
import com.example.webutvjava.service.BeverageService;
import com.example.webutvjava.service.CompanyService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
public class WebutvJavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebutvJavaApplication.class, args);
    }

    @Bean
    CommandLineRunner init(BeverageService beverageService, CompanyService companyService){
        return args -> {


            Company carlsberg = new Company("Carlsberg");
            Company cocacola = new Company("Coca-Cola Company");
            Company drpeppercompany = new Company("DR.Pepper Company");

            companyService.save(carlsberg);
            companyService.save(cocacola);
            companyService.save(drpeppercompany);


            List<Beverage> bevList = List.of(
                    new Beverage("Coca-cola", cocacola, 50, 21.50, "Standard coca-cola, traditional recipe"),
                    new Beverage("Fanta", cocacola, 50, 22.50, "Orange soda"),
                    new Beverage("Dr Pepper", drpeppercompany, 50, 23.50, "Gamer fuel"),
                    new Beverage("Pepsi Max", carlsberg, 33, 24.50, "Wannabe coca-cola. No sugar"),
                    new Beverage("Sprite", cocacola, 50, 25.50, "Drinkmix")
            );

            beverageService.saveAll(bevList);

        };
    }

}
