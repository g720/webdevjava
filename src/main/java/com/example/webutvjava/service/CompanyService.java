package com.example.webutvjava.service;

import com.example.webutvjava.domain.entity.Company;
import com.example.webutvjava.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.List;
//simple repo the Company entity
@Service
public class CompanyService {
	CompanyRepository companyRepository;

	public CompanyService(CompanyRepository companyRepository) {
		this.companyRepository = companyRepository;
	}

	public List<Company> findAll(){return companyRepository.findAll();}

	public void save(Company company) {
		companyRepository.save(company);
	}

	public void deleteById(int id) {companyRepository.deleteById(id);}

	public void saveAll(List<Company> company) {
		companyRepository.saveAll(company);
	}

	public Company editOne(Company changed, int companyId){
		Company existingCompany = companyRepository.findById(companyId).orElseThrow();

		if(!changed.getName().isEmpty() || !changed.getName().isBlank()){
			existingCompany.setName(changed.getName());
		}

		companyRepository.save(existingCompany);

		return existingCompany;
	}
}
