package com.example.webutvjava.service;

import com.example.webutvjava.domain.entity.Beverage;
import com.example.webutvjava.repository.BeverageRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BeverageService {
    BeverageRepository beverageRepository;

    public BeverageService(BeverageRepository beverageRepository) {
        this.beverageRepository = beverageRepository;
    }

    public List<Beverage> findAll(){
        return beverageRepository.findAll();
    }

    public void deleteById(int id) {beverageRepository.deleteById(id);}

    public List<Beverage> findByCompanyName(String companyName){
        return beverageRepository.findBeverageByCompany_Name(companyName);
    }

    public void save(Beverage beverage){
        beverageRepository.save(beverage);
    }

    public void saveAll(List<Beverage> beverageList) {
        beverageRepository.saveAll(beverageList);
    }

    public Beverage editOne(Beverage changed, int id){
        Beverage existingBeverage = beverageRepository.findById(id).orElseThrow();

        if(!changed.getName().isEmpty() || !changed.getName().isBlank()){
            existingBeverage.setName(changed.getName());
        }
        if(!changed.getDescription().isEmpty() || !changed.getDescription().isBlank()){
            existingBeverage.setDescription(changed.getDescription());
        }
        if(changed.getAmount() != 0){
            existingBeverage.setAmount(changed.getAmount());
        }
        if(changed.getPrice() != 0){
            existingBeverage.setPrice(changed.getPrice());
        }
        if(changed.getCompany() != null){
            existingBeverage.setCompany(changed.getCompany());
        }

        beverageRepository.save(existingBeverage);
        return existingBeverage;
    }

}
