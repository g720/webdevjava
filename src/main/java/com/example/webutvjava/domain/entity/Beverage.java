package com.example.webutvjava.domain.entity;

import com.example.webutvjava.domain.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.text.DecimalFormat;

@Getter // Lombok getters and setters
@Setter
@Entity
@NoArgsConstructor // Lombok all argument constructor
@AllArgsConstructor // Lombok all argument constructor
public class Beverage extends AbstractEntity {


    @Column
    @NotBlank
    private String name; //Unique?


    @ManyToOne
    @JoinColumn(name = "company_id")
    @NotNull
    @JsonIgnoreProperties({"products"}) // To avoid recursive calls
    private Company company; //Many products only got relation to one company

    private int amount;
    private double price;
    private String description;

    public String getFormattedPrice() {
        String formattedPrice = String.format("%.2f kr", price);
        return formattedPrice;
    }


}
