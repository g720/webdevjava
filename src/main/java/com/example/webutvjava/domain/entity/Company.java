package com.example.webutvjava.domain.entity;

import com.example.webutvjava.domain.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@Entity
public class Company extends AbstractEntity {

	@NotBlank
	private String name;

	@OneToMany(mappedBy = "company") // One company can have relations with many products
	private List<Beverage> products = new LinkedList<>();

	public Company(String name) {
		this.name = name;
	}
}
