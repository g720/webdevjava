package com.example.webutvjava.component;

import com.example.webutvjava.domain.entity.Company;
import com.example.webutvjava.view.CompanyView;
import com.helger.commons.mutable.MutableBoolean;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.textfield.TextField;

public class ConfirmationDialog extends Dialog {
    Paragraph message = new Paragraph();
    Button okBtn = new Button("Confirm");
    Button cancelBnt = new Button("Cancel");

    public ConfirmationDialog(CompanyView companyView, Company company, String dialogText){
        message.setText(dialogText);

        okBtn.addClickListener(e -> {
            companyView.deleteCompanyAfterConfirmation(company);
            this.close();
        });

        cancelBnt.addClickListener(e -> {
            this.close();
        });

        add(message, okBtn, cancelBnt);

    }

}
