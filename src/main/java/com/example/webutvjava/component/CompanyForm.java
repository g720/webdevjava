package com.example.webutvjava.component;

import com.example.webutvjava.domain.entity.Company;
import com.example.webutvjava.service.CompanyService;
import com.example.webutvjava.view.CompanyView;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;

public class CompanyForm extends FormLayout {
    TextField name = new TextField("Company name");
    Button saveBtn = new Button("Save");

    Binder<Company> binder = new BeanValidationBinder<>(Company.class);

    CompanyService companyService;
    CompanyView companyView;

    public CompanyForm(CompanyService companyService, CompanyView companyView){
        binder.bindInstanceFields(this);
        this.companyService = companyService;
        this.companyView = companyView;
        setVisible(false);

        saveBtn.addClickListener(e -> {
            handleSave();
        });

        add(name, saveBtn);
    }

    private void handleSave() {
        Company company = binder.validate().getBinder().getBean();

        if(company.getId() == null){
            companyService.save(company);
        } else{
            companyService.editOne(company, company.getId());
        }
        setCompany(null);
        companyView.updateGrid();

        this.getParent().ifPresent(component -> {
            if(component instanceof Dialog){
                ((Dialog) component).close();
            }
        });
    }

    public void setCompany(Company company) {
        if(company != null){
            binder.setBean(company);
            setVisible(true);
        } else{
            setVisible(false);
        }
    }
}
