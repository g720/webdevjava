package com.example.webutvjava.component;

import com.example.webutvjava.domain.entity.Beverage;
import com.example.webutvjava.domain.entity.Company;
import com.example.webutvjava.repository.CompanyRepository;
import com.example.webutvjava.service.BeverageService;
import com.example.webutvjava.service.CompanyService;
import com.example.webutvjava.view.BeverageView;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

public class BeverageForm extends FormLayout {
    CompanyService companyService;
    BeverageService beverageService;
    BeverageView beverageView;
    Binder<Beverage> binder = new BeanValidationBinder<>(Beverage.class);
    Button saveBtn = new Button("Save");

    TextField name = new TextField("Beverage name");
    IntegerField amount = new IntegerField("Amount");
    NumberField price = new NumberField("Price");
    TextArea description = new TextArea("Description");
    Select<Company> company = new Select<>();

    public BeverageForm(CompanyService companyService, BeverageService beverageService, BeverageView beverageView){
        this.companyService = companyService;
        this.beverageService = beverageService;
        this.beverageView = beverageView;

        company.setLabel("Choose company");
        company.setPlaceholder("Already chosen company will persist");
        company.setItemLabelGenerator(Company::getName);
        List<Company> companyList = companyService.findAll();
        company.setItems(companyList);

        saveBtn.addClickListener(evt -> {
            handleSave();
        });

        binder.bindInstanceFields(this);
        setVisible(false);

        add(name, amount, price, description, company, saveBtn);
    }

    public void handleSave(){
        Beverage beverage = binder.validate().getBinder().getBean();

        if(beverage.getId() == null){
            beverageService.save(beverage);
        } else {
            beverageService.editOne(beverage, beverage.getId());
        }
        setBeverage(null);
        beverageView.updateGrid();

        this.getParent().ifPresent(component -> {
            if(component instanceof Dialog){
                ((Dialog) component).close();
            }
        });
    }

    public void setBeverage(Beverage beverage){
        if(beverage != null){
            binder.setBean(beverage);
            setVisible(true);
        } else{
            setVisible(false);
        }
    }

}
