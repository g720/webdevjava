package com.example.webutvjava.view;

import com.example.webutvjava.component.BeverageForm;
import com.example.webutvjava.component.CompanyForm;
import com.example.webutvjava.domain.entity.Beverage;
import com.example.webutvjava.domain.entity.Company;
import com.example.webutvjava.security.SecurityUtils;
import com.example.webutvjava.service.BeverageService;
import com.example.webutvjava.service.CompanyService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.annotation.security.PermitAll;
import java.util.List;
import java.util.stream.Collectors;

@PermitAll // ALL Logged in users can access
@RouteAlias(value = "", layout = AppView.class) //Add this as endpoint on empty URI
@Route(value = "/beverages", layout = AppView.class)
public class BeverageView extends VerticalLayout {

    BeverageService beverageService;
    CompanyService companyService;
    Grid<Beverage> grid;
    BeverageForm beverageForm;

    public BeverageView(BeverageService beverageService, CompanyService companyService){
        this.beverageService = beverageService;
        this.companyService = companyService;
        this.beverageForm = new BeverageForm(companyService, beverageService, this);
        this.grid = new Grid<Beverage>(Beverage.class, true);
        
        grid.setColumns("name", "description"); //Getting fields by passing their names, then setting them as columns
        grid.addColumn(beverage -> beverage.getCompany().getName()).setHeader("Company"); //Adding an object(Company) as a column, setting header as Company
        grid.addColumn(Beverage::getFormattedPrice).setHeader("Price").setTextAlign(ColumnTextAlign.END);

        grid.asSingleSelect().addValueChangeListener(e -> {
            beverageForm.setBeverage(e.getValue());
        });

        if(SecurityUtils.checkRoles().contains("ROLE_ADMIN")) {
            grid.addComponentColumn(beverage -> {
                Button btn = new Button(new Icon(VaadinIcon.CLOSE),
                        evt -> {
                            beverageService.deleteById(beverage.getId());
                            updateGrid();
                        });
                return btn;
            });
        }

        updateGrid();

        HorizontalLayout hl = new HorizontalLayout(grid, beverageForm);
        hl.setSizeFull();


            Button addBeverageBtn = new Button("Add beverage");
            addBeverageBtn.addClickListener(e -> {
                Dialog dialog = new Dialog();
                BeverageForm beverageForm = new BeverageForm(companyService, beverageService, this);
                Beverage beverage = new Beverage();
                beverageForm.setBeverage(beverage);
                dialog.add(beverageForm);
                dialog.open();
            });

        add(addBeverageBtn,hl);
    }

    public void updateGrid(){
        grid.setItems(beverageService.findAll());
    }
}
