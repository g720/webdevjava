package com.example.webutvjava.view;

import com.example.webutvjava.security.SecurityService;
import com.example.webutvjava.security.SecurityUtils;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.RouterLink;

public class AppView extends AppLayout {

    public AppView(SecurityService service){
        HorizontalLayout appHeader = new HorizontalLayout();
        Button logoutButton = new Button("Logout", e -> service.logout()); // Button which calls logout from SecurityService

        logoutButton.getStyle().set("margin-left", "auto");
        logoutButton.addClassName("mr-m"); //TODO trying to get some space to the right of button, but still float it right/flex-end


        H1 beverage_data_app  = new H1 ("Beverage Data App");

        appHeader.add(beverage_data_app,logoutButton);
        appHeader.addClassNames("m-l");
        appHeader.setWidthFull();
        appHeader.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER); //Sets drawer and logo alignment to CENTER

        addToNavbar(appHeader);

        RouterLink beverageView = new RouterLink("Beverages", BeverageView.class);
        RouterLink companyView = new RouterLink("Companies", CompanyView.class);
        //RouterLink shoppingListView = new RouterLink("Shoppinglist", BeverageView.class);

        addToDrawer(new VerticalLayout(beverageView, companyView));
    }

}
