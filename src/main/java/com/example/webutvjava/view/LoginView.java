package com.example.webutvjava.view;

import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.*;

@Route("login") // URI route
@PageTitle("Login") //Title on tab
public class LoginView extends VerticalLayout implements BeforeEnterObserver { // Implements BeforeEnterObserver, let us use the beforeEnter-method

	private LoginForm login = new LoginForm();

	public LoginView() {
		// Adding class for later CSS-styling
		addClassName("login-view");

		// Center Horizontally and Vertically
		setAlignItems(Alignment.CENTER);
		setJustifyContentMode(JustifyContentMode.CENTER);

		// Set size to full
		setSizeFull();

		login.setAction("login"); // "Sets the path where to send the form-data when a form is submitted.", setting it to beverages-path

		// Header for login
		H1 headerlogin = new H1("Vaadin Beverages Fullstack App!");

		// Adding components
		add(
				headerlogin,
				login
		);
	}

	@Override
	public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
		if (beforeEnterEvent.getLocation()
				.getQueryParameters()
				.getParameters()
				.containsKey("error")) { // Check if the url has an error parameter, if so we show that error.
			login.setError(true); // Setting error to true on LoginForm
		}


	}


}
