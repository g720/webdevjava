package com.example.webutvjava.view;

import com.example.webutvjava.component.CompanyForm;
import com.example.webutvjava.component.ConfirmationDialog;
import com.example.webutvjava.domain.entity.Beverage;
import com.example.webutvjava.domain.entity.Company;
import com.example.webutvjava.service.BeverageService;
import com.example.webutvjava.service.CompanyService;
import com.helger.commons.mutable.MutableBoolean;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.router.Route;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.security.auth.callback.ConfirmationCallback;
import java.io.DataInput;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@PermitAll //All Logged in users can try to access
@Secured("ROLE_ADMIN") //adding custom access control
@Route(value = "/companies", layout = AppView.class)
public class CompanyView extends VerticalLayout {
    CompanyService companyService;
    BeverageService beverageService;
    Grid<Company> grid;
    CompanyForm companyForm;

    public CompanyView(CompanyService companyService, BeverageService beverageService){
        this.companyService = companyService;
        this.beverageService = beverageService;
        this.grid = new Grid<Company>(Company.class, false);
        this.companyForm = new CompanyForm(companyService, this);

        HorizontalLayout hl = new HorizontalLayout();
        hl.setSizeFull();

        grid.addColumn(company -> company.getName());

        grid.asSingleSelect().addValueChangeListener(e -> {
            companyForm.setCompany(e.getValue());
        });

        grid.setDetailsVisibleOnClick(true);
        grid.setItemDetailsRenderer(getCompanyDetails());

        grid.addComponentColumn(company -> {
            Button btn = new Button(new Icon(VaadinIcon.CLOSE),
                    evt -> {
                        //This opens a confirmation dialog that runs the deletefunction if confirmation.
                        //Tried to change a MutableBoolean but then i think we'll need some sort of Await function.
                        ConfirmationDialog confirmationDialog = new ConfirmationDialog(this, company, "This delete will also delete all related beverages. Confirm?");
                        confirmationDialog.open();
                    });
            return btn;
        });

        updateGrid();

        Button addCompanyBtn = new Button("Add company");
        addCompanyBtn.addClickListener(e -> {
            Dialog dialog = new Dialog();
            CompanyForm companyForm = new CompanyForm(companyService, this);
            Company company = new Company();
            companyForm.setCompany(company);
            dialog.add(companyForm);
            dialog.open();
        });

        hl.add(grid, companyForm);
        add(addCompanyBtn, hl);

    }

    public void updateGrid() {
        grid.setItems(companyService.findAll());
    }

    //This is finding company by Name because I forgot that the id was in AbstractEntity.
    public ComponentRenderer getCompanyDetails(){
        ComponentRenderer<VerticalLayout, Company> CR = new ComponentRenderer<>(item ->{
            VerticalLayout vl = new VerticalLayout();
            List<Beverage> bList = beverageService.findByCompanyName(item.getName());
            Grid<Beverage> bGrid = new Grid<Beverage>(Beverage.class, false);
            bGrid.addColumn(beverage -> beverage.getName()).setHeader("Beverage");
            bGrid.addColumn(beverage -> beverage.getPrice()).setHeader("Price");
            bGrid.addColumn(beverage -> beverage.getDescription()).setHeader("Description");

            //Deprecated - Find solution
            bGrid.setHeightByRows(true);
            bGrid.setItems(bList);
            vl.add(bGrid);
            return vl;
        });
        return CR;
    }

    public void deleteCompanyAfterConfirmation(Company company){
        List<Beverage> bList = beverageService.findByCompanyName(company.getName());
        bList.forEach(beverage -> {
            beverageService.deleteById(beverage.getId());
        });

        companyService.deleteById(company.getId());
        updateGrid();
    }
}
