package com.example.webutvjava.security;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SecurityUtils {

	public static boolean isAccessGranted(Class<?> securedClass) {
		// Allow if no roles are required.
		Secured secured = AnnotationUtils.findAnnotation(securedClass, Secured.class);
		if (secured == null) {
			return true; //Give access if the view is not protected.
		}

		// lookup needed role in user roles
		List<String> allowedRoles = Arrays.asList(secured.value());
		Authentication userAuthentication = SecurityContextHolder.getContext().getAuthentication();
		return userAuthentication.getAuthorities().stream() //Iterate all authorities the user has and check if access can be granted.
				.map(GrantedAuthority::getAuthority)
				.anyMatch(allowedRoles::contains);
	}

	public static boolean isUserLoggedIn() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication(); // Get authentication from context
		return authentication != null // If not null
				&& !(authentication instanceof AnonymousAuthenticationToken) // and if not instance of AnonymousAuthenticationToken "Do not forget to check for AnonymousAuthenticationToken Spring Security assigns by default to non-logged in users."
				&& authentication.isAuthenticated(); // and isAuthenticated
	}

	public static List<String> checkRoles(){
		Authentication userAuthentication = SecurityContextHolder.getContext().getAuthentication();
		return userAuthentication.getAuthorities().stream().map(GrantedAuthority -> GrantedAuthority.getAuthority()).collect(Collectors.toList());
	}


}
