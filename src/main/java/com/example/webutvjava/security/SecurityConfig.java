package com.example.webutvjava.security;

import com.example.webutvjava.view.LoginView;
import com.vaadin.flow.spring.security.VaadinWebSecurityConfigurerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.List;

@EnableWebSecurity //Enables custom Spring boot Security and make this the configuration class
@Configuration
public class SecurityConfig extends VaadinWebSecurityConfigurerAdapter {



	@Override
	protected void configure(HttpSecurity http) throws Exception { // Main method when implementing our custom security
		super.configure(http); //Need to call superclass so the parent-class can do its necessary things
		setLoginView(http, LoginView.class); //Set login view to our own customised LoginView, passing the HttpSecurity object as first argument
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring()// Spring Security should completely ignore URLs starting with /resources/
				.antMatchers("/resources/**");
		super.configure(web);
	}

	@Bean
	@Override //Configure two users for the system, by defining userDetailsService.
	protected UserDetailsService userDetailsService() {

//		SimpleGrantedAuthority admin = new SimpleGrantedAuthority("ADMIN");
//		User admin1 = new User("Admin", "{noop}1234", List.of(admin)); 	// !!Just two different ways to add users!!

//		return new InMemoryUserDetailsManager(  //Need another AuthenticationProvider for any actual implementation, but for all intents and purposes this works for now
//				User.withUsername("user") 		// !!Just two different ways to add users!!
//						.password("{noop}1234").roles("USER").build(),admin1);


		// typical logged in user with some privileges
		UserDetails normalUser =
				User.withUsername("user")
						.password("{noop}1234")
						.roles("USER")
						.build();

		// admin user with all privileges
		UserDetails adminUser =
				User.withUsername("admin")
						.password("{noop}1234")
						.roles("USER", "ADMIN")
						.build();


		return new InMemoryUserDetailsManager(normalUser, adminUser);



	}
}
