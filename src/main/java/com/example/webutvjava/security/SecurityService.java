package com.example.webutvjava.security;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.server.VaadinServletRequest;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Component;

@Component //Component annotation so we can autowire it
public class SecurityService {

	// Method for logoutbutton
	public void logout() {
		UI.getCurrent().getPage().setLocation("/"); // Navigate the user to the empty path after clicking logout
		SecurityContextLogoutHandler logoutHandler = new SecurityContextLogoutHandler();
		logoutHandler.logout(VaadinServletRequest.getCurrent().getHttpServletRequest(), null,null); //(request, response, authentication), only requires the request which we can get from vaadinservlet

	}
}
