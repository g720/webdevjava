package com.example.webutvjava.security;

import com.example.webutvjava.view.LoginView;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.NotFoundException;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;
import org.springframework.stereotype.Component;

@Component //Component annotation so we can autowire it
public class ConfigureUIServiceInitListener implements VaadinServiceInitListener {

	@Override
	public void serviceInit(ServiceInitEvent event) {
		event.getSource().addUIInitListener(uiEvent -> {
			final UI ui = uiEvent.getUI();
			ui.addBeforeEnterListener(this::beforeEnter);
		});
	}

	private void beforeEnter(BeforeEnterEvent event) {
		if(!SecurityUtils.isAccessGranted(event.getNavigationTarget())) { // isAccessGranted NOT to the navigationTarget
			if(SecurityUtils.isUserLoggedIn()) {  //if User is logged in
				event.rerouteToError(NotFoundException.class);  //Fake exception
			} else {
				event.rerouteTo(LoginView.class); //if user is not logged in, reroute to LoginView
			}
		}

	}
}
