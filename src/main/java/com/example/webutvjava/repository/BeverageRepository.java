package com.example.webutvjava.repository;

import com.example.webutvjava.domain.entity.Beverage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BeverageRepository extends JpaRepository<Beverage, Integer> {

    public List<Beverage> findBeverageByCompany_Name(String companyName);

}
